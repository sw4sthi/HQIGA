using Random
using Debugger

# Setting up Parameters
noOfProcessors = 10
noOfTasks = 100
bitSize::Integer = ceil(log2(noOfProcessors))
arrivalRate = 0.05
maxTime = arrivalRate * noOfTasks

struct Task
    arrivalTime :: Float64
    readyTime :: Float64
    computationTime :: Float64
    deadline :: Float64
end

function randInRange(a, b, randomDevice)
    return a + rand(randomDevice)*(b-a)
end

# Function to Generate Tasks
function generateTasks(noOfTasks, noOfProcessors)
    randomDevice = MersenneTwister(2134)
    tasks :: Vector{Task} = []
    for _ in 1:noOfTasks
        at = randInRange(0, maxTime, randomDevice)
        rt = randInRange(at, at+4*noOfProcessors,randomDevice)
        ct = randInRange(20, 40, randomDevice)
        d = randInRange(rt + 2*ct, rt+3*ct, randomDevice)
        task = Task(at, rt, ct, d)
        push!(tasks, task)
    end
    return tasks
end
    
mutable struct Processor
    dispatchQueue :: Vector{Task}
    nextStartTime :: Float64
end
    
struct quantumChromosome
    chromosomeSize :: Integer
    qubits ::Vector{Vector{Float64}}
    quantumChromosome(cS) =  new(cS, [[1/sqrt(2), 1/sqrt(2)] for _ in 1:bitSize*cS])
    quantumChromosome(cS, qubits) =  new(cS, qubits)
end

# Measurement Function
function measure(qubit, randomDevice)
    rand(randomDevice) < qubit[1]^2 ? 0 : 1
end

# Function to decode a Quantum Chromosome into a Schedule
function decode(chromosome::quantumChromosome, bitSize)
    rDev = MersenneTwister(2134)
    decoded = []
    decodedBits = []
    for i in 1:bitSize:chromosome.chromosomeSize*bitSize
        bits = [measure(q, rDev) for q in chromosome.qubits[i:i+bitSize-1]]
        val = sum(reverse(bits) .* 2 .^ (0:bitSize-1))+1
        push!(decoded, val)
        decodedBits=cat(decodedBits, bits, dims=1)
    end
    return decodedBits, decoded
end

# Function to prune arbitrary schedules into valid schedules
function permutationPruning(schedule, noOfProcessors)
    if length(schedule) < noOfProcessors
        result = [s % noOfProcessors for s in schedule]
        result = [s==0 ? noOfProcessors : s for s in result]
        if length(unique(result)) == length(result)
            return result
        else
            unqItems = unique(result)
            for i in result
                indices = findall(x->x==i, result)        
                if length(indices) > 1
                    for j in indices[2:end]
                        while (result[j] ∈ unqItems)
                            result[j] += 1
                            result[j] = result[j] % noOfProcessors 
                            if result[j] == 0 result[j] = 1 end
                        end
                        push!(unqItems, result[j])
                    end
                end
            end
            return result
        end
    end
    d = Dict()
    s = sort(schedule)
    for i in unique(schedule)
        indices = findall(x->x==i, s)
        push!(d, i=>indices)
    end
    result = zeros(Int, length(schedule)) 
    for i in unique(schedule)
        indices = findall(x->x==i, schedule)
        result[indices] = d[i]
    end
    result = [if i>noOfProcessors i%noOfProcessors else i end for i in result]
    result = [if i==0 noOfProcessors else i end for i in result]
    return result
end

# Function to assign tasks to a processor as per a schedule and obtain its objective.
function objectiveFunction(schedule, tasks, processors::Vector{Processor})
    for (s, t) in zip(schedule, tasks)
        push!(processors[s].dispatchQueue, t)
    end
    fitness = 0
    for p in processors
        if !isempty(p.dispatchQueue)
            fitness += fitnessFunction(deepcopy(p))
            empty!(p.dispatchQueue)
        end
    end
    return fitness
end

#Evaluates fitness of an assignment of tasks.
function fitnessFunction(processor::Processor)
    f = 0
    sort!(processor.dispatchQueue, by = x->x.deadline)
    for t in processor.dispatchQueue
        finishTime = processor.nextStartTime + t.computationTime
        if t.readyTime <= processor.nextStartTime && finishTime <= t.deadline
            f += 1
            processor.nextStartTime = finishTime
        end
    end
    return f
end

function rotMatrix(theta)
    return [cos(theta) -sin(theta); sin(theta) cos(theta)]
end

# Quantum Rotation Gate based on a lookup table.
function applyRotation(chromosome::quantumChromosome, measuredChromosome, best, cfitness, bfitness, randomDevice)
    resultQubits::Vector{Vector{Float64}} = []
    isbest = cfitness >= bfitness
    for (r, y, b) in zip(chromosome.qubits, measuredChromosome, best)
        alpha, beta = r[1], r[2]
        theta = 0
        if !isbest
            if y == 1 && b == 0
                theta = 0.1*pi
                if alpha > 0 && beta > 0
                    theta = theta * -1
                elseif alpha < 0 && beta<0
                    theta = theta * 1
                elseif alpha == 0
                    theta = theta * rand(randomDevice, [-1,1])
                elseif alpha<0 && beta>0
                    theta = theta * 0
                end
            elseif y==1 && b==1
                theta = 0.005*pi
                if alpha > 0 && beta > 0
                    theta = theta * 1
                elseif alpha < 0 && beta<0
                    theta = theta * -1
                elseif alpha == 0
                    theta = theta * 0
                elseif alpha<0 && beta>0
                    theta = theta * rand(randomDevice, [-1,1])
                end
            end
        else
            if y==1 && b==1
                theta = 0.025*pi
                if alpha > 0 && beta > 0
                    theta = theta * 1
                elseif alpha < 0 && beta<0
                    theta = theta * -1
                elseif alpha == 0
                    theta = theta * 0
                elseif alpha<0 && beta>0
                    theta = theta * rand(randomDevice, [-1,1])
                end
            end
            if y==0 && b==1
                theta = 0.005*pi
                if alpha > 0 && beta > 0
                    theta = theta * -1
                elseif alpha < 0 && beta<0
                    theta = theta * 1
                elseif alpha == 0
                    theta = theta * rand(randomDevice, [-1,1])
                elseif alpha<0 && beta>0
                    theta = theta * 0
                end
            end
            if y==1 && b==0
                theta = 0.025*pi
                if alpha > 0 && beta > 0
                    theta = theta * 1
                elseif alpha < 0 && beta<0
                    theta = theta * -1
                elseif alpha == 0
                    theta = theta * 0
                elseif alpha<0 && beta>0
                    theta = theta * rand(randomDevice, [-1,1])
                end
            end
        end
        r = rotMatrix(theta)*r
        push!(resultQubits, r)
    end
    resultChromosome = quantumChromosome(chromosome.chromosomeSize, resultQubits)
    return resultChromosome
end

# Main Function Simulating our Quantum Genetic Algorithm
function QGA(readyTasks, currentState, populationSize, maxGenerations)    
    @bp
    rDev = MersenneTwister(2134)
    schedule = Vector{Integer}(undef, length(readyTasks))
    population = [quantumChromosome(length(readyTasks)) for _ in 1:populationSize]
    bestFitness = 0
    for _ in 1:maxGenerations
        generatedSchedules = []
        measuredChromosome = []
        for p in population
            bits, values = decode(p, bitSize)
            push!(generatedSchedules, values)
            push!(measuredChromosome, bits)
        end
        generatedSchedules = [permutationPruning(s, noOfProcessors) for s in generatedSchedules]
        fitnesses = [objectiveFunction(s, readyTasks, deepcopy(currentState)) for s in generatedSchedules]
        bestFitness, bestIndex = findmax(fitnesses)
        schedule = generatedSchedules[bestIndex]
        bestChromosome = measuredChromosome[bestIndex]
        population = [applyRotation(p, m, bestChromosome, f, bestFitness, rDev) for (p, m, f) in zip(population, measuredChromosome,fitnesses)]
    end
    return bestFitness, schedule    
end

# Helper function to check whether a task is scheduled or not.
function isScheduled(task::Task, processor::Processor)
    if task.readyTime <= processor.nextStartTime && processor.nextStartTime+task.computationTime <= task.deadline
        return true
    end
    return false
end

# Main Loop 
function main()
    tasks :: Vector{Task} = generateTasks(noOfTasks, noOfProcessors)
    tasks = sort(tasks, by = x -> x.readyTime)
    meanReadyTime = (maxTime+4*noOfProcessors)/noOfTasks
    tIncrement = floor(Int, 15*meanReadyTime)

    # Assign Initial Tasks Randomly to Processors
    processors = [Processor([], 0) for _ in 1:noOfProcessors]
    assignments = randperm!([i for i in 1:noOfProcessors])
    for (i, t) in enumerate(tasks[1:noOfProcessors])
        idx = assignments[i]
        processors[idx].nextStartTime += t.readyTime+t.computationTime
    end 
    tasks = tasks[noOfProcessors:end]
    scheduledTasks = noOfProcessors

    time::Float64 = 0
    readyQueue = []
    while !isempty(tasks)
        sort!(readyQueue, by = x->x.deadline)
        readyQueue = filter(x->x.readyTime <= time, tasks)
        tasks = filter(x->x.readyTime > time, tasks)
        if !isempty(readyQueue)
            for p in processors
                if p.nextStartTime <= time
                    p.nextStartTime = time
                end
            end
            fitness, schedule = QGA(deepcopy(readyQueue), deepcopy(processors), 20, 150)
            println(schedule, " with fitness ", fitness)
            scheduledTasks += fitness
            poppedTasks = []
            for (s, t) in zip(schedule, readyQueue)
                if isScheduled(t, processors[s])
                    processors[s].nextStartTime += t.computationTime
                    push!(poppedTasks, t)
                end
                empty!(readyQueue)
            end
            readyQueue = setdiff!(readyQueue, poppedTasks)
        end
        println(scheduledTasks)
        time += tIncrement
    end
    return 
end
